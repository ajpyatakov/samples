package com.bitbucket.nesterovilya.samples.springangulargradlesample.service.mapper;

import com.bitbucket.nesterovilya.samples.springangulargradlesample.config.PersistenceConfig;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.config.TestConfig;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.generator.CertificateObjectGenerator;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.Certificate;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.enumerations.Category;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.enumerations.Gender;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Ilya Nesterov
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PersistenceConfig.class, TestConfig.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:db/initial-data.xml")
public class CertificateMapperTest {

    @Autowired
    private CertificateMapper certificateMapper;

    @Autowired
    private CertificateObjectGenerator certificateObjectGenerator;

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSelectCertificates() throws Exception {

        List<Certificate> certificates = certificateMapper.selectCertificates();

        assertNotNull(certificates);
        assertEquals(certificates.size(), 6);
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSelectCertificatesByNameFilter() throws Exception {

        String nameFilter = "Можайский Леонид Якубович%";
        List<Certificate> certificates = certificateMapper.selectCertificatesByNameFilter(nameFilter);
        assertNotNull(certificates);
        assertEquals(certificates.size(), 1);

        nameFilter = "Можайский Леонид%";
        certificates = certificateMapper.selectCertificatesByNameFilter(nameFilter);
        assertNotNull(certificates);
        assertEquals(certificates.size(), 1);

        nameFilter = "Можайский%";
        certificates = certificateMapper.selectCertificatesByNameFilter(nameFilter);
        assertNotNull(certificates);
        assertEquals(certificates.size(), 2);

        nameFilter = "А%";
        certificates = certificateMapper.selectCertificatesByNameFilter(nameFilter);
        assertNotNull(certificates);
        assertEquals(certificates.size(), 2);
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testSelectCertificateByGuid() throws Exception {

        Certificate certificate = certificateMapper.selectCertificateByGuid("7238e4c6-68e4-4a04-97b9-ff9a78bcca88");

        assertNotNull(certificate);
        assertEquals(certificate.getGuid(), "7238e4c6-68e4-4a04-97b9-ff9a78bcca88");
        assertEquals(certificate.getName(), "Сидоров Петр Иванович");
        assertEquals(certificate.getDateOfBirth(), new Date(Timestamp.valueOf("1987-06-04 00:00:00").getTime()));
        assertEquals(certificate.getGender(), Gender.MALE);
        assertEquals(certificate.getCategory(), Category.C);

    }

    @Test
    @ExpectedDatabase(value = "classpath:db/after-update-certificate.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testUpdateCertificate() throws Exception {

        Certificate certificateForUpdate = certificateObjectGenerator.generateCertificateForUpdate();
        int result = certificateMapper.updateCertificate(certificateForUpdate);

        assertEquals(result, 1);
    }


}