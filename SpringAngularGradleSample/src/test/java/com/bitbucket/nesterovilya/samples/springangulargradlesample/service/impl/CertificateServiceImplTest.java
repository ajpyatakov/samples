package com.bitbucket.nesterovilya.samples.springangulargradlesample.service.impl;

import com.bitbucket.nesterovilya.samples.springangulargradlesample.config.RootConfig;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.config.TestConfig;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.generator.CertificateObjectGenerator;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.CertificateService;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.CertificateNotFoundException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.FieldValidationException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.WrongNameFilterException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.Certificate;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author Ilya Nesterov
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {RootConfig.class, TestConfig.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
@DatabaseSetup("classpath:db/initial-data.xml")
public class CertificateServiceImplTest {

    @Autowired
    private CertificateService certificateService;

    @Autowired
    private CertificateObjectGenerator certificateObjectGenerator;

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetAllCertificates() throws Exception {
        certificateService.getAllCertificates();
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetCertificatesByNameFilter_Success() throws Exception {
        String filter = "Можайский";
        certificateService.getCertificatesByNameFilter(filter);
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetCertificatesByNameFilter_ShouldThrowWrongNameFilterException() throws Exception {
        String filter = "Можайский12";
        try {
            certificateService.getCertificatesByNameFilter(filter);
            fail("WrongNameFilterException expected");
        } catch (WrongNameFilterException ex) {}

        filter = "Можайский%";
        try {
            certificateService.getCertificatesByNameFilter(filter);
            fail("WrongNameFilterException expected");
        } catch (WrongNameFilterException ex) {}

        filter = "Мо";
        try {
            certificateService.getCertificatesByNameFilter(filter);
            fail("WrongNameFilterException expected");
        } catch (WrongNameFilterException ex) {}

        filter = "Мо";
        try {
            certificateService.getCertificatesByNameFilter(filter);
            fail("WrongNameFilterException expected");
        } catch (WrongNameFilterException ex) {}

        filter = "     ";
        try {
            certificateService.getCertificatesByNameFilter(filter);
            fail("WrongNameFilterException expected");
        } catch (WrongNameFilterException ex) {}
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetCertificateByGuid_Success() throws Exception {
        certificateService.getCertificateByGuid("7238e4c6-68e4-4a04-97b9-ff9a78bcca88");
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testGetCertificateByGuid_ShouldThrowCertificateNotFoundException() throws Exception {
        try {
            certificateService.getCertificateByGuid("fake");
            fail("CertificateNotFoundException expected");
        } catch (CertificateNotFoundException ex) {}
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/after-update-certificate.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testUpdateCertificate_Success() throws Exception {
        certificateService.updateCertificate(certificateObjectGenerator.generateCertificateForUpdate());
    }

    @Test
    @ExpectedDatabase(value = "classpath:db/initial-data.xml", assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    public void testUpdateCertificate_ShouldThrowFieldValidationException() throws Exception {
        try {
            certificateService.updateCertificate(new Certificate());
            fail("FieldValidationException expected");
        } catch (FieldValidationException ex) {}
    }
}