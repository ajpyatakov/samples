package com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions;

/**
 * @author Ilya Nesterov
 */
public class CertificateNotFoundException extends Exception {

    public CertificateNotFoundException(String message) {
        super(message);
    }
}
