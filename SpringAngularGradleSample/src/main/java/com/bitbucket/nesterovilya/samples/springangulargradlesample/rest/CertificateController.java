package com.bitbucket.nesterovilya.samples.springangulargradlesample.rest;

import com.bitbucket.nesterovilya.samples.springangulargradlesample.rest.dto.DataResponse;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.rest.dto.ErrorResponse;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.rest.dto.MessageResponse;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.rest.dto.Response;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.CertificateService;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.CertificateNotFoundException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.FieldValidationException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.WrongNameFilterException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.Certificate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Ilya Nesterov
 */

// CrossOrigin - только для тестирования!!!
@CrossOrigin
@RestController
@RequestMapping(CertificateRestAPI.ROOT)
public class CertificateController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CertificateController.class);

    @Autowired
    private CertificateService certificateService;

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    public Response getCertificates() {
        LOGGER.debug("GET /certificate");
        List<Certificate> response = certificateService.getAllCertificates();
        LOGGER.debug("Response: {}",response);
        return new DataResponse(response);
    }

    @RequestMapping(
            value = CertificateRestAPI.GET_CERTIFICATES_BY_FILTER,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    public Response getCertificatesByFilter(@RequestParam(value = "filter", required = true) String filter) throws Exception{
        LOGGER.debug("GET /certificate/search?filter={}",filter);
        List<Certificate> response = certificateService.getCertificatesByNameFilter(filter);
        LOGGER.debug("Response: {}",response);
        return new DataResponse(response);
    }

    @RequestMapping(
            value = CertificateRestAPI.GET_CERTIFICATE_BY_GUID,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    public Response getCertificateByGuid(@PathVariable("guid") String guid) throws Exception{
        LOGGER.debug("GET /certificate/{}",guid);
        Certificate response = certificateService.getCertificateByGuid(guid);
        LOGGER.debug("Response: {}",response);
        return new DataResponse(response);
    }

    @RequestMapping(
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    @ResponseStatus(HttpStatus.OK)
    public Response updateCertificate(@RequestBody Certificate certificate) throws Exception{
        LOGGER.debug("PUT /certificate RequestBody: {}",certificate);
        certificateService.updateCertificate(certificate);
        return new MessageResponse("Страховой полис " + certificate.getGuid() + " успешно обновлен.");
    }

    @ExceptionHandler(WrongNameFilterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response handleWrongNameFilterException(WrongNameFilterException ex) {
        LOGGER.debug("WrongNameFilterException was handled in REST layer: ",ex);
        return new ErrorResponse("WrongNameFilterException",ex.getMessage(), null);
    }

    @ExceptionHandler(CertificateNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Response handleCertificateNotFoundException(CertificateNotFoundException ex) {
        LOGGER.debug("CertificateNotFoundException was handled in REST layer: ",ex);
        return new ErrorResponse("CertificateNotFoundException",ex.getMessage(), null);
    }

    @ExceptionHandler(FieldValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Response handleFieldValidationException(FieldValidationException ex) {
        LOGGER.debug("FieldValidationException was handled in REST layer: ",ex);
        return new ErrorResponse("FieldValidationException",ex.getMessage(), null);
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response handleException(Exception ex) {
        LOGGER.debug("Exception was handled in REST layer: ",ex);
        return new ErrorResponse("Exception",ex.getMessage(), ex.getStackTrace());
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Response handleRuntimeException(RuntimeException ex) {
        LOGGER.debug("RuntimeException was handled in REST layer: ",ex);
        return new ErrorResponse("RuntimeException",ex.getMessage(), ex.getStackTrace());
    }


}
