package com.bitbucket.nesterovilya.samples.springangulargradlesample.rest;

/**
 * @author Ilya Nesterov
 */
public class CertificateRestAPI {

    public static final String ROOT = "/certificate";
    public static final String GET_CERTIFICATES_BY_FILTER = "/search";
    public static final String GET_CERTIFICATE_BY_GUID = "/{guid}";
}
