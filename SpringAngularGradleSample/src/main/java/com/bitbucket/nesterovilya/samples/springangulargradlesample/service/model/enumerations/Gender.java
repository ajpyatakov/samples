package com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.enumerations;

/**
 * Пол
 * @author Ilya Nesterov
 */
public enum Gender {

    MALE("М"),
    FEMALE("Ж");

    private String title;

    Gender(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
