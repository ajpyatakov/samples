package com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model;

import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.enumerations.Category;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.enumerations.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.time.DateUtils;

import java.util.Calendar;
import java.util.Date;

/**
 * Страховой полис водителя
 * @author Ilya Nesterov
 */
public class Certificate {

    /**
     * Уникальный идентификатор полиса
     */
    private String guid;

    /**
     * ФИО водителя
     */
    private String name;

    /**
     * Дата рождения
     */
    private Date dateOfBirth;

    /**
     * Возраст
     */
    private Integer age;

    /**
     * Пол
     */
    private Gender gender;

    /**
     * Категория водителя
     */
    private Category category;

    public static int calculateAge(Date dateOfBirth) {
        Calendar dateOfBirthday = DateUtils.toCalendar(dateOfBirth);
        // включаем день рождения в подсчет
        dateOfBirthday.add(Calendar.DAY_OF_MONTH, -1);

        Calendar now = Calendar.getInstance();

        int age = now.get(Calendar.YEAR) - dateOfBirthday.get(Calendar.YEAR);
        // учитываем возможный недостаток дней в текущем году
        if (now.get(Calendar.DAY_OF_YEAR) <= dateOfBirthday.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        return age;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Integer getAge() {
        if (age == null) {
            age = calculateAge(dateOfBirth);
        }
        return age;
    }

    public void setAge(Integer age) {
        this.age = calculateAge(dateOfBirth);
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("guid", guid)
                .append("name", name)
                .append("dateOfBirth", dateOfBirth)
                .append("age", age)
                .append("gender", gender)
                .append("category", category)
                .toString();
    }
}
