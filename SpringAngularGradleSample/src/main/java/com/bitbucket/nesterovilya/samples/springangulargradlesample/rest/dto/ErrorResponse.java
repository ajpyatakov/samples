package com.bitbucket.nesterovilya.samples.springangulargradlesample.rest.dto;

/**
 * @author Ilya Nesterov
 */
public class ErrorResponse implements Response {

    /**
     * Наименование класса исключения
     */
    private String exception;

    /**
     * Сообщение об ошибке
     */
    private String message;

    /**
     * Стектрейс ошибки
     */
    private StackTraceElement[] stackTraceElements;

    public ErrorResponse() {}

    public ErrorResponse(String exception, String message, StackTraceElement[] stackTraceElements) {
        this.exception = exception;
        this.message = message;
        this.stackTraceElements = stackTraceElements;
    }

    public String getException() {
        return exception;
    }

    public void setException(String exception) {
        this.exception = exception;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StackTraceElement[] getStackTraceElements() {
        return stackTraceElements;
    }

    public void setStackTraceElements(StackTraceElement[] stackTraceElements) {
        this.stackTraceElements = stackTraceElements;
    }
}
