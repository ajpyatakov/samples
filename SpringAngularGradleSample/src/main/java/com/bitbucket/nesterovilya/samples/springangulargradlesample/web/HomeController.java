package com.bitbucket.nesterovilya.samples.springangulargradlesample.web;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Ilya Nesterov
 */
@Controller
public class HomeController {

    @RequestMapping(value = "/",
            method = RequestMethod.GET,
            produces = MediaType.TEXT_HTML_VALUE)
    public String getIndex(){
        return "index";
    }
}
