package com.bitbucket.nesterovilya.samples.springangulargradlesample.config;

import com.bitbucket.nesterovilya.samples.springangulargradlesample.util.Mapper;
import org.apache.ibatis.session.AutoMappingBehavior;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * @author Ilya Nesterov
 */

@Configuration
@Import({DataSourceConfig.class, UtilConfig.class})
@ComponentScan("com.bitbucket.nesterovilya.samples.springangulargradlesample.service.mapper")
@MapperScan(basePackages = "com.bitbucket.nesterovilya.samples.springangulargradlesample.service.mapper", annotationClass = Mapper.class)
@EnableTransactionManagement
public class PersistenceConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersistenceConfig.class);

    @Autowired
    @Bean(name = "sessionFactory")
    public SqlSessionFactory getSessionFactory(DataSource dataSource) throws Exception{

        if (dataSource == null) return null;
        try {
            SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
            factoryBean.setDataSource(dataSource);
            SqlSessionFactory sessionFactory = factoryBean.getObject();
            org.apache.ibatis.session.Configuration configuration = sessionFactory.getConfiguration();
            configuration.setAutoMappingBehavior(AutoMappingBehavior.NONE);
            return sessionFactory;
        }
        catch(Exception ex){
            LOGGER.error("Ошибка в построении SqlSessionFactory!",ex);
            return null;
        }
    }

    @Autowired
    @Bean(name = "transactionManager")
    public DataSourceTransactionManager getTransactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }
}
