package com.bitbucket.nesterovilya.samples.springangulargradlesample.service.impl;

import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.CertificateService;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.CertificateNotFoundException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.FieldValidationException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions.WrongNameFilterException;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.mapper.CertificateMapper;
import com.bitbucket.nesterovilya.samples.springangulargradlesample.service.model.Certificate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Ilya Nesterov
 */

@Service
public class CertificateServiceImpl implements CertificateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CertificateServiceImpl.class);

    private static final Pattern NAME_FILTER_PATTERN = Pattern.compile("^[a-zA-Zа-яА-Я ]+$");

    @Autowired
    private CertificateMapper certificateMapper;


    @Override
    public List<Certificate> getAllCertificates() {
        LOGGER.debug("Запрос на выборку всех страховых полисов.");
        return certificateMapper.selectCertificates();
    }

    @Override
    public List<Certificate> getCertificatesByNameFilter(String filter) throws WrongNameFilterException {
        LOGGER.debug("Запрос на выборку страховых полисов, фио владельцев которых начинаются на {}.", filter);

        filter = filter.trim();

        if (filter.length() < 3) {
            LOGGER.debug("Запрос на выборку страховых полисов по фильтру {} отклонен. Причина: недостаточная длина фильтра. Ожидалось: не менее 3. Фактическая длина: {}.", filter, filter.length());
            throw new WrongNameFilterException("Длина строки фильтра должна быть минимум 3 символа");
        }
        if (!NAME_FILTER_PATTERN.matcher(filter).matches()) {
            LOGGER.debug("Запрос на выборку страховых полисов по фильтру {} отклонен. Причина: несоответствие поискового фильтра требованиям. Допустимо использовать только пробелы, символы кириллицы и латинского алфавита.", filter);
            throw new WrongNameFilterException("Несоответствие поискового фильтра требованиям. Допустимо использовать только пробелы, символы кириллицы и латинского алфавита");
        }

        return certificateMapper.selectCertificatesByNameFilter(filter + "%");
    }

    @Override
    public Certificate getCertificateByGuid(String guid) throws CertificateNotFoundException {
        LOGGER.debug("Запрос на выборку страхового полиса по идентификатору {}.",guid);
        Certificate certificate = certificateMapper.selectCertificateByGuid(guid);

        if (certificate == null) {
            LOGGER.debug("Страховой полис с идентификатором {} не найден.",guid);
            throw new CertificateNotFoundException("Страховой полис с идентификатором " + guid + " не найден.");
        }

        return certificate;
    }

    @Override
    public void updateCertificate(Certificate updatedCertificate) throws FieldValidationException {
        LOGGER.debug("Запрос на обновление страхового полиса по идентификатору {}.", updatedCertificate.getGuid());
        if (updatedCertificate.getGuid() == null) {
            LOGGER.debug("Запрос на обновление страхового полиса отклонен. Причина: идентификатор сущности null.");
            throw new FieldValidationException("Уникальный идентификатор обновляемой сущности не может быть null");
        }

        int result = certificateMapper.updateCertificate(updatedCertificate);

        if (result != 1) {
            LOGGER.error("Непредвиденная ошибка во время обновления страхового полиса {}.", updatedCertificate.getGuid());
            throw new IllegalStateException("Непредвиденная ошибка во время обновления страхового полиса " + updatedCertificate.getGuid());
        }
    }
}
