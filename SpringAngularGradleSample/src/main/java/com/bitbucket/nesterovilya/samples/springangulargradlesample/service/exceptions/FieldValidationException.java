package com.bitbucket.nesterovilya.samples.springangulargradlesample.service.exceptions;

/**
 * @author Ilya Nesterov
 */
public class FieldValidationException extends Exception{

    public FieldValidationException(String message) {
        super(message);
    }
}
