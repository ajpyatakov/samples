package com.bitbucket.nesterovilya.samples.springangulargradlesample.config;

import com.bitbucket.nesterovilya.samples.springangulargradlesample.util.Mapper;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Import;

/**
 * @author Ilya Nesterov
 */

@Configuration
@Import(PersistenceConfig.class)
@ComponentScan( basePackages = {"com.bitbucket.nesterovilya.samples.springangulargradlesample.service"},
        excludeFilters={
                @ComponentScan.Filter(type= FilterType.ANNOTATION, value=Mapper.class)
        })
public class RootConfig {
}
