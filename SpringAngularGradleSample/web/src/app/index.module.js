(function() {
  'use strict';

  angular
    .module('webapp', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'restangular',
      'ui.router',
      'ngMaterial',
      'md.data.table'
    ]);

})();
