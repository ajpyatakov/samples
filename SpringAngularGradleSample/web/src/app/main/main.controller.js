(function() {
  'use strict';

  angular
    .module('webapp')
    .controller('MainController', ['$scope','$mdDialog','Restangular', MainController]);

  /** @ngInject */
  function MainController($scope,$mdDialog,Restangular) {

    $scope.selected = [];

    $scope.selectedCertificate = null;

    $scope.datePickerOptions = {
      maxDate : new Date(),
      minDate : new Date(1910, 0, 1)
    };

    $scope.categories = [
      {
        name: "A",
        title: "Мотоциклы"
      },
      {
        name: "A1",
        title: "Легкие мотоциклы"
      },
      {
        name: "B",
        title: "Легковые автомобили"
      },
      {
        name: "BE",
        title: "Легковые автомобили с прицепом"
      },
      {
        name: "B1",
        title: "Трициклы"
      },
      {
        name: "C",
        title: "Грузовые автомобили"
      },
      {
        name: "CE",
        title: "Грузовые автомобили с прицепом"
      },
      {
        name: "C1",
        title: "Легкие грузовики"
      },
      {
        name: "C1E",
        title: "Легкие грузовики с прицепом"
      },
      {
        name: "D",
        title: "Автобусы"
      },
      {
        name: "DE",
        title: "Автобусы с прицепом"
      },
      {
        name: "D1",
        title: "Небольшие автобусы"
      },
      {
        name: "D1E",
        title: "Небольшие автобусы с прицепом"
      },
      {
        name: "M",
        title: "Мопеды"
      },
      {
        name: "Tm",
        title: "Трамваи"
      },
      {
        name: "Tb",
        title: "Троллейбусы"
      }
    ];

    $scope.pagination = {
      label: "{page: 'Страница:', rowsPerPage: 'Результатов на странице:', of: 'из'}",
      options: [1,3,5,10]
    };


    $scope.certificates = {
      data: [],
      count: 0,
      deleteCertificate: function (elem) {
        if ($scope.selectedCertificate && $scope.selectedCertificate.guid === elem.guid) {
          $scope.selectedCertificate = null;
        }
        var index = $scope.certificates.data.indexOf(elem);
        $scope.certificates.data.splice(index, 1);
        $scope.certificates.count = $scope.certificates.data.length;
      },
      changeCertificate: function (certificate) {
        $scope.selectedCertificate = angular.copy(certificate);
        $scope.selectedCertificate.dateOfBirth = new Date($scope.selectedCertificate.dateOfBirth);
        var fio = certificate.name.split(" ");
        fio = _.filter(fio, function (part) {
          return part;
        });
        if (fio[0]) {
          $scope.selectedCertificate.firstName = fio[0];
        }
        if (fio[1]) {
          $scope.selectedCertificate.lastName = fio[1];
        }
        if (fio[2]) {
          $scope.selectedCertificate.patronymic = fio[2];
        }
      },
      saveCertificate: function () {
        var founded = _.find($scope.certificates.data, {guid: $scope.selectedCertificate.guid});
        var updatedCertificate = angular.copy($scope.selectedCertificate);
        $scope.selectedCertificate = null;
        $scope.search.searchText = "";
        updatedCertificate.name = updatedCertificate.firstName + " " + updatedCertificate.lastName + " " + updatedCertificate.patronymic;
        updatedCertificate.dateOfBirth = updatedCertificate.dateOfBirth.getTime();

        if (founded) {
          angular.merge(founded, updatedCertificate);
          Restangular.one("certificate").customPUT(founded, '', {}, { ContentType:'application/json'}).then(function(data){
            $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Системное сообщение')
                .textContent('Данные успешно обновлены')
                .ariaLabel('Success')
                .ok('ОК')
            );
          },
          function (error) {
            var message = error.data.message || "";
            $mdDialog.show(
              $mdDialog.alert()
                .parent(angular.element(document.querySelector('#popupContainer')))
                .clickOutsideToClose(true)
                .title('Ошибка')
                .textContent('Ошибка при обновлении данных.\n' + message)
                .ariaLabel('Error')
                .ok('ОК')
            );
          });
        } else {
          $mdDialog.show(
            $mdDialog.alert()
              .parent(angular.element(document.querySelector('#popupContainer')))
              .clickOutsideToClose(true)
              .title('Ошибка')
              .textContent('Непредвиденная ошибка при обновлении данных.')
              .ariaLabel('Error')
              .ok('ОК')
          );
        }

      }

    };

    $scope.onChangeDate = function() {
      $scope.selectedCertificate.age = calcAge($scope.selectedCertificate.dateOfBirth);
    };

    $scope.parseGender = function(gender) {
      if (gender === "MALE") {
        return "М";
      } else if (gender === "FEMALE") {
        return "Ж";
      }
    };

    function calcAge(dateOfBirth) {
      var birthDate = new Date(dateOfBirth);
      birthDate.setDate(birthDate.getDate() - 1);
      var now = new Date();
      var age = now.getFullYear() - birthDate.getFullYear();
      return now.setFullYear(1972) < birthDate.setFullYear(1972) ? age - 1 : age;
    }

    $scope.query = {
      order: 'name',
      limit: 5,
      page: 1
    };


    /*
      Функции и модель для поиска страховых полисов
     */

    $scope.search = {
      simulateQuery : false,
      isDisabled : false,
      repos : [],
      querySearch : querySearch,
      selectedItemChange: selectedItemChange
    };

    /**
     * Callback, срабатывающий на изменении выбранного элемента
     */
    function selectedItemChange(item) {
      if (!item) return;
      var founded = _.find($scope.certificates.data, {guid: item.guid});
      if (!founded) {
        $scope.certificates.data.push(item);
        $scope.certificates.count = $scope.certificates.data.length;
      }
    }

    /**
     * Поиск страховых полисов
     */
    function querySearch(query) {
      query = query.trim();
      if (!/^[a-zA-Zа-яА-Я ]+$/.test(query)) {
        $mdDialog.show(
          $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title('Неверный поисковый запрос')
            .textContent('Поисковая строка может сожержать только пробелы, \nсимволы кириллицы и латинского алфавита.')
            .ariaLabel('Error')
            .ok('Закрыть')
        );
        $scope.search.searchText = "";
        return;
      }
      return Restangular.one("certificate/search").get({filter: query}).then(function (response) {
        return response.data;
      },
      function (error) {
        console.log(error);
      }
      );
    }

  }
})();
